# Pipeline

This repository contains two things so far:

## Stylua base image
CI for building a simple image containing [`StyLua`](https://github.com/JohnnyMorganz/StyLua).  
The image is published at `registry.gitlab.com/warsaw-revamped-oss/modding/pipeline/stylua`.

## Pipeline definition for Lua projects
A simple pipeline definition for Lua projects, containing a `StyLua` check task.
To use it in a project, set _Settings_ -> _CI/CD_ -> _CI/CD configuration file_ to `.lua-gitlab-ci.yml@warsaw-revamped-oss/modding/pipeline`.